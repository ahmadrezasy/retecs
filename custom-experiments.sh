./retecs.py --agent 'network' --reward 'binary' --scenario-provider 'paintcontrol' --quiet --comparable
./retecs.py --agent 'network' --reward 'failcount' --scenario-provider 'paintcontrol' --quiet --comparable
./retecs.py --agent 'network' --reward 'timerank' --scenario-provider 'paintcontrol' --quiet --comparable
./retecs.py --agent 'network' --reward 'tcfail' --scenario-provider 'paintcontrol' --quiet --comparable
./retecs.py --agent 'network' --reward 'binary' --scenario-provider 'iofrol' --quiet --comparable
./retecs.py --agent 'network' --reward 'failcount' --scenario-provider 'iofrol' --quiet --comparable
./retecs.py --agent 'network' --reward 'timerank' --scenario-provider 'iofrol' --quiet --comparable
./retecs.py --agent 'network' --reward 'tcfail' --scenario-provider 'iofrol' --quiet --comparable